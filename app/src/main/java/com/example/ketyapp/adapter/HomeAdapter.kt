package com.example.ketyapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.ketyapp.databinding.ItemListBinding
import com.example.ketyapp.view.home.HomeFragmentDirections

class HomeAdapter(private val navDetails: (String) -> Unit) :
    RecyclerView.Adapter<HomeAdapter.HomeViewHolder>() {

    private var home = mutableListOf<String>()

    /* Set up View Holder */
    // This method inflates card layout items for Recycler View
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val binding = ItemListBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return HomeViewHolder(binding)
    }

    // This method sets the data to specific views of card items.
    // It also handles methods related to clicks on items of Recycler view.
    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val homeData = home[position]
        holder.loadCategory(homeData)
        holder.returnImage().setOnClickListener {
            navDetails(homeData)
        }
    }

    // This method returns the length of the RecyclerView.
    override fun getItemCount(): Int {
        return home.size
    }

    // === Custom Function to pass object[it] ===
    // viewModel gets category and assigns data to home variable
    fun addCategory(item: List<String>) {
        this.home = item.toMutableList()
        notifyDataSetChanged()
    }

    class HomeViewHolder(
        private val binding: ItemListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        // This function gets the data from home member variable / position and
        // passes data as an argument to this function
        fun loadCategory(home: String) {
            binding.tvText.text = home
        }
        fun returnImage() : ConstraintLayout {
            return binding.ivCard
        }
    }
}