package com.example.ketyapp.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ketyapp.adapter.HomeAdapter
import com.example.ketyapp.databinding.FragmentHomeBinding
import com.example.ketyapp.viewmodel.HomeViewModel

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val homeViewModel by viewModels<HomeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        initDisplay(binding.rvCategory, 0)
//        initDisplay(binding.rvSkin, 1)
        binding.rvCategory.initDisplay(0)
        binding.rvSkin.initDisplay(1)
    }

    // === Custom Functions ===
    private fun RecyclerView.initDisplay(idx: Int) {
        with(this) {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = HomeAdapter(::navigate).apply {
                homeViewModel.getCategory()
                homeViewModel.state.observe(viewLifecycleOwner) {
                    addCategory(it[idx])
                }
            }
        }
    }

    fun navigate(category: String) {
        val detailFrag = HomeFragmentDirections.goToDetailFragment(category)
        findNavController().navigate(detailFrag)
    }

}