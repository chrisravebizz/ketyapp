package com.example.ketyapp.model.remote

interface HomeService {
    suspend fun getCategory(): List<List<String>>

}