package com.example.ketyapp.model

import com.example.ketyapp.model.remote.HomeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object HomeRepo {

    private val categoryList = object : HomeService {
        override suspend fun getCategory(): List<List<String>> {
            return listOf(
                listOf(
                    "RANK",
                    "HOT",
                    "LOVED",
                    "SECRETS",
                    "GROUPS",
                    "COUPLES",
                    "DISC GOLF",
                    "SOULMATE",
                    "RANDOM",
                    "GAMER"
                ),
                listOf(
                    "NORMAL",
                    "DRY",
                    "OILY",
                    "COMBINE",
                    "WRINKLY",
                    "SMOOTH",
                    "DIMPLE",
                    "ROUGH",
                    "TAN",
                    "FRECKLE"
                )
            )
        }
    }

    suspend fun getCategory(): List<List<String>> = withContext(Dispatchers.IO) {
        return@withContext categoryList.getCategory()
    }
}