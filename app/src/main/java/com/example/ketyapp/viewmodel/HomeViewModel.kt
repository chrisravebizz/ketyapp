package com.example.ketyapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ketyapp.model.HomeRepo
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    // Instantiate repo
    private val repo = HomeRepo

    // Initialize mutable live data
    private val _state = MutableLiveData<List<List<String>>>()
    val state: LiveData<List<List<String>>> get() = _state

    // Create function to launch new coroutine without blocking the main thread
    // Assign repo function to mutable live data variable
    fun getCategory() {
        viewModelScope.launch {
            val category = repo.getCategory()
            _state.value = category
        }
    }
}